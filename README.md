# Research-Tools
Collection of tools aimed at assisting research efforts. Developed sparingly and at random. Feel free to use/abuse/modify and otherwise do anything with this code.

## Batch Overview
Perl script used to create an overview of a large collection of research papers for quick introductory reading.

Check out the [miner here](BatchOverview/).

## Research Graph
Graph visualization of research papers references and citations.

Check out the [grapher here](Research-Graph/)
