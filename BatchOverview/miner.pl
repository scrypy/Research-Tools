#!/usr/bin/perl

print "Path to collection to parse: (with trailing slash, relative or absolute)\n";
my $collectionPATH = <STDIN>;
chomp $collectionPATH;
print $collectionPATH;

opendir my $dir, "$collectionPATH" or die "Cannot open directory: $!";
my @files = readdir $dir;

foreach my $filepath (@files) {
  print "$filepath\n";
  my @stripped=split(/\./,$filepath);
  my $outfile=$collectionPATH+$stripped[0]+".extracted.txt";
  my $file="$collectionPATH$filepath";
  print "$file\n";
  `pdf2txt.py -o "$outfile" "$file"`;
}
closedir $dir;
print "done";
