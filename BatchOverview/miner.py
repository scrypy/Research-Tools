#batch overview miner - pull raw text from pdfs
#uses code from project:pdf2txt.py (many thanks for gr8 tool)

import sys,os,fnmatch
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from pdfminer.image import ImageWriter

# main
def main():
    # debug option
    debug = 0
    # input option
    password = ''
    pagenos = set()
    maxpages = 0
    # output option
    outfile = None
    outtype = None
    imagewriter = None
    rotation = 0
    stripcontrol = False
    layoutmode = 'normal'
    codec = 'utf-8'
    pageno = 1
    scale = 1
    caching = True
    showpageno = True
    laparams = LAParams()

    PDFDocument.debug = debug
    PDFParser.debug = debug
    CMapDB.debug = debug
    PDFPageInterpreter.debug = debug
    #
    rsrcmgr = PDFResourceManager(caching=caching)

    folder_path_input = str(raw_input('enter folder path:'))
    files_to_parse=[]
    if os.path.isdir(folder_path_input):
        files = os.listdir(folder_path_input)
        pattern = "*.pdf"
        for fl in files:
            if fnmatch.fnmatch(fl,pattern):
                files_to_parse.append(fl)
    else:
        print('not a folder')
        return

    for fname in files_to_parse:
        print('>> Beginning to parse file: '+fname)
        try:
            outfile = {}
            outfile['text'] = file(folder_path_input+"output/{}.txt".format(fname.split('.')[0]), 'w')
            #outfile['xml'] = file(folder_path_input+"output/{}.xml".format(fname.split('.')[0]), 'w')
            #outfile['tag'] = file("{}.tag".format(fname.split('.')[0]), 'w')

            devices = {}
            devices['text']=TextConverter(rsrcmgr, outfile['text'], codec=codec, laparams=laparams,imagewriter=imagewriter)
            #devices['xml']=XMLConverter(rsrcmgr, outfile['xml'], codec=codec, laparams=laparams,imagewriter=imagewriter,stripcontrol=stripcontrol)
            #devices['tag']=TagExtractor(rsrcmgr, outfile['tag'], codec=codec)
            fp = file(folder_path_input+fname, 'rb')
            for key in devices:
                interpreter = PDFPageInterpreter(rsrcmgr, devices[key])
                for page in PDFPage.get_pages(fp, pagenos,
                                              maxpages=maxpages,caching=True,
                                              check_extractable=True):
                    page.rotate = (page.rotate+rotation) % 360
                    interpreter.process_page(page)
            fp.close()

            for key in devices:
                devices[key].close()
            for key in outfile:
                outfile[key].close()
        except Exception as e:
            print('ERROR:::'+str(e))

    return

if __name__ == '__main__': sys.exit(main())
