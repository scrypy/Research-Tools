# Py Research Miner
Research paper extractor in order to efficiently comb through vast amounts of
previous research.

uses the python pdfminer library and it's pdf2txt.py file.

* Overview Generator - parse batches of papers and generates an overview file
containing the abstract, and conclusion sections from each paper.

To Run:
* change directory to pdfminer/
* run `sudo python setup.py install` (needs python2.7)
* return to BatchOverview and run `python miner.py`
* select relative or absolute path for folder containing papers to batch process (include the trailing slash)
