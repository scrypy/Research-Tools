# Extractors
* [PDF Bib tools](http://chriswarbo.net/projects/pdf-tools.html)
* [Bibtex from pdf](https://academia.stackexchange.com/questions/15504/is-there-an-open-source-tool-for-producing-bibtex-entries-from-paper-pdfs)
* [.bibtex from pdf](https://tex.stackexchange.com/questions/226357/is-it-possible-to-extract-the-bibliography-from-a-pdf-file-as-a-bibtex)
* [pdfextract](https://waterprogramming.wordpress.com/2014/07/24/pdfextract-get-a-list-of-bibtex-references-from-a-scholarly-pdf/)
* [pdfextract github](https://github.com/CrossRef/pdfextract)


# Graphing
* [neo4j](neo4j.org)
* * [installing neo4j on ubu 16](http://www.exegetic.biz/blog/2016/09/installing-neo4j-ubuntu-16-04/)
* [neo4j graph viz](https://neo4j.com/developer/guide-data-visualization/)
* [Gephi](gephi.com)
* [Gephi blog: neo4j](https://gephi.wordpress.com/tag/neo4j/)
* [neo4j & Gephi](https://www.packtpub.com/mapt/book/big_data_and_business_intelligence/9781783287253/7/ch07lvl1sec75/the-neo4j-graph-with-gephi)

# Papers
* [The Structure and Dynamics of Co-Citation Clusters: A Multiple-Perspective Co-Citation Analysis](https://arxiv.org/abs/1002.1985)

# Analyzing
* [Co-citation analysis](https://en.wikipedia.org/wiki/Co-citation)
* [Bibliometrics](https://en.wikipedia.org/wiki/Bibliometrics)
* [Scientometrics](https://en.wikipedia.org/wiki/Scientometrics)
