# Research-Graph
The goal of this project is to make an effective workflow for both graphing and visualizing the referential relations between academic papers. I want to use [Gephi](https://gephi.org/) to visualize the results because I have had past experience graphing networks with it. Ideally each node would reference a BibTex citation and show relationships from there.
* [links](./links.md)

## Thoughts
* Build on the fly OR from previous collections
* The below reference extractors are for extracting information fro PDF files of an existing collection. There needs to be a way to collect and store citations as you gather. (i.e. UW's library allows you to collect and export)

## Saving Data
There is the possibility of saving everything into a [Neo4j](neo4j.com) database but that should be an advanced feature for very large workloads.

## Reference Extractors
* [CERMINE](https://github.com/CeON/CERMINE)
* [PDF bib tools](http://chriswarbo.net/projects/pdf-tools.html)
* [Stack Exchange Answer](https://academia.stackexchange.com/questions/15504/is-there-an-open-source-tool-for-producing-bibtex-entries-from-paper-pdfs)
* [Stack Answer 2](https://tex.stackexchange.com/questions/226357/is-it-possible-to-extract-the-bibliography-from-a-pdf-file-as-a-bibtex)
* [PDFExtract](https://waterprogramming.wordpress.com/2014/07/24/pdfextract-get-a-list-of-bibtex-references-from-a-scholarly-pdf/)
* [pdfkit](https://pypi.python.org/pypi/pdfkit/0.4.1)
